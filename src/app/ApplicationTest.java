package app;

import model.Address;
import model.Employee;
import model.HourlyEmployee;
import model.SalariedEmployee;


public class ApplicationTest {

	public static void main(String[] args) {
		
		//address
		Address a1 = new Address("Ugljese Terzina", "49", "Serbia");
		Address a2 = new Address("Sabacka", "6", "Mongolija");
		//employee
		Employee e1 = new Employee("Zarko", "Djurdjev", "127", a1);
		Employee e2 = new Employee("Petar", "Maric", "131", a2);
		System.out.println(e1);
		System.out.println("------------------------------------------------------------------------");
		System.out.println(e2);
		
		//HourlyEmployee
		
		HourlyEmployee h1 = new HourlyEmployee("Slavko", "Djuric", "132", a1, 100.0, 40);
		HourlyEmployee h2 = new HourlyEmployee("Ivan", "Blagojevic", "133", a1, 200.0, 50);
		System.out.println("------------------------------------------------------------------------");
		
		
		System.out.println(h1);
		h1.getPaymentAmount();
		System.out.println("------------------------------------------------------------------------");
		System.out.println(h2);
		h2.getPaymentAmount();
		
		//SalariedEmployees
		SalariedEmployee s1 = new SalariedEmployee("Jelena", "Zelenovic", "135", a2, 40000.0);
		SalariedEmployee s2 = new SalariedEmployee("Dragana", "Popovic", "136", a1, 35000.0);
		
		System.out.println("-------------------------------");
		System.out.println(s1);
		s1.getPaymentAmount();
		System.out.println("-------------------------------");
		System.out.println(s2);
		s2.getPaymentAmount();
		
	}
}
