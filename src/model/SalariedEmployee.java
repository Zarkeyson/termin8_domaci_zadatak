package model;

public class SalariedEmployee extends Employee{
	private double weeklySalary;
	
	public SalariedEmployee(){}


	public SalariedEmployee(String firstName, String lastName, String socialSecurityNumber, Address address,
			double weeklySalary) {
		super(firstName, lastName, socialSecurityNumber, address);
		this.weeklySalary = weeklySalary;
	}



	public double getWeeklySalary() {
		return weeklySalary;
	}

	public void setWeeklySalary(double weeklySalary) {
		this.weeklySalary = weeklySalary;
	}
	
	


	@Override
	public double getPaymentAmount() {
		// TODO Auto-generated method stub
		System.out.println("Weekly salary is: "+ this.weeklySalary);
		return weeklySalary;
	}


	@Override
	public String toString() {
		return "SalariedEmployee [weeklySalary=" + weeklySalary + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", socialSecurityNumber=" + socialSecurityNumber + ", address=" + address + "]";
	}
	
	
	
}
