package model;

public interface Payable {
	public double getPaymentAmount();
}
