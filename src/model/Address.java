package model;

public class Address {
	protected String street;
	protected String number;
	protected String country;
	
	public Address(){}

	public Address(String street, String number, String country) {
		super();
		this.street = street;
		this.number = number;
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Address [street=" + street + ", number=" + number + ", country=" + country + ", toString()="
				+ super.toString() + "]";
	}
	
	
	
}
