package model;

public class HourlyEmployee extends Employee{
	private double salaryPerHour;
	private double hours;
	
	public HourlyEmployee(){}

	

	public HourlyEmployee(String firstName, String lastName, String socialSecurityNumber, Address address,
			double salaryPerHour, double hours) {
		super(firstName, lastName, socialSecurityNumber, address);
		this.salaryPerHour = salaryPerHour;
		this.hours = hours;
	}



	public double getSalaryPerHour() {
		return salaryPerHour;
	}

	public void setSalaryPerHour(double salaryPerHour) {
		this.salaryPerHour = salaryPerHour;
	}

	public double getHours() {
		return hours;
	}

	public void setHours(double hours) {
		this.hours = hours;
	}

	

	@Override
	public String toString() {
		return "HourlyEmployee [salaryPerHour=" + salaryPerHour + ", hours=" + hours + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", socialSecurityNumber=" + socialSecurityNumber + ", address=" + address
				+ "]";
	}



	@Override
	public double getPaymentAmount() {
		double salary;
		double overTimeHours;
		if (hours <= 40) {
			salary = hours * salaryPerHour;
			System.out.println("Salary is: " + salary);
		} else {
			overTimeHours = (hours - 40) * (salaryPerHour * 1.5);
			salary = 40 * salaryPerHour + overTimeHours;
			System.out.println("Salary with overtime is: " + salary);
		}
		return salary;
	}
	
	
	
	
}
